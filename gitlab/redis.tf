resource "random_password" "redis_password" {
  length  = 16
  special = false
}

data "aws_subnet" "redis_subnet" {
  count = max(length(var.redis_subnet_ids), 2)
  id    = var.redis_subnet_ids[count.index]
}

module "redis" {
  source = "git::https://github.com/cloudposse/terraform-aws-elasticache-redis.git?ref=0.38.0"

  name                    = var.name
  vpc_id                  = data.aws_subnet.redis_subnet.0.vpc_id
  availability_zones      = toset(data.aws_subnet.redis_subnet.*.availability_zone)
  subnets                 = toset(data.aws_subnet.redis_subnet.*.id)
  allowed_security_groups = [var.eks_wokers_security_group_id]
  instance_type           = var.redis_instance_type
  family                  = "redis5.0"
  engine_version          = "5.0.6"

  at_rest_encryption_enabled = false
  transit_encryption_enabled = false
  // TODO Add support for encryption and Auth token
  //  auth_token                 = random_password.redis_password.result

  // GitLab Does Not Support Cluster Mode = true
  cluster_mode_enabled       = false
  automatic_failover_enabled = false

  apply_immediately = true
}
