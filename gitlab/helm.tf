locals {
  # According to https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/values.yaml
  helm_gitlab_sets = {
    "edition"                           = "ee"
    "global.hosts.domain"               = var.domain
    "global.gitlab.license.secret"      = kubernetes_secret.gitlab_license.metadata[0].name
    "global.gitlab.license.key"         = local.license_kubernetes_secret_key
    "global.initialRootPassword.secret" = kubernetes_secret.gitlab_root_password.metadata[0].name
    "global.initialRootPassword.key"    = local.root_password_kubernetes_secret_key

    # External Postgres, Redis and Object Storage Config. All should be false.
    "postgresql.install"   = local.use_external_postgres ? "false" : "true"
    "redis.install"        = local.use_external_redis ? "false" : "true"
    "global.minio.enabled" = local.use_external_object_store ? "false" : "true"

    "global.registry.bucket"  = element(regex("(\\S*${local.s3_bucket_fragments["registry"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "registry.storage.secret" = kubernetes_secret.gitlab_docker_registry_object_store_connection.metadata[0].name
    "registry.storage.key"    = local.object_store_connection_kubernetes_secret_key

    "global.appConfig.backups.bucket"                        = element(regex("(\\S*${local.s3_bucket_fragments["backups"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "global.appConfig.backups.tmpBucket"                     = element(regex("(\\S*${local.s3_bucket_fragments["tmp"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "gitlab.task-runner.backups.objectStorage.config.secret" = kubernetes_secret.gitlab_docker_backups_object_store_connection.metadata[0].name
    "gitlab.task-runner.backups.objectStorage.config.key"    = local.object_store_connection_kubernetes_secret_key

    # CertManager
    "global.ingress.configureCertmanager" = "true"
    "certmanager-issuer.email" : var.certmanager_issuer_email

    # Gitaly
    # According to https://docs.gitlab.com/charts/charts/globals.html#internal
    "gitlab.gitaly.persistence.storageClass" = kubernetes_storage_class.gitaly_storage_class.metadata[0].name
    "gitlab.gitaly.persistence.size"         = "${var.gitaly_storage_size_gigabytes}Gi"

    "global.antiAffinity" = "hard"

    # Misc
    "global.appConfig.defaultCanCreateGroup"   = "false"
    "global.appConfig.usernameChangingEnabled" = "false"

    # Other
    "prometheus.install" = "false"
  }

  derived_k8s_tolerations = {
    key   = length(var.k8s_toleration_label) != 0 ? lookup(element(var.k8s_toleration_label, 0), "key", "") : ""
    value = length(var.k8s_toleration_label) != 0 ? lookup(element(var.k8s_toleration_label, 0), "value", "") : ""
  }

  derived_k8s_ci_tolerations = {
    key   = length(var.k8s_ci_toleration_label) != 0 ? lookup(element(var.k8s_ci_toleration_label, 0), "key", "") : ""
    value = length(var.k8s_ci_toleration_label) != 0 ? lookup(element(var.k8s_ci_toleration_label, 0), "value", "") : ""
  }

  helm_toleration_sets = {
    "gitlab.gitaly.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.gitaly.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.gitaly.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.gitaly.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab.gitlab-exporter.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.gitlab-exporter.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.gitlab-exporter.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.gitlab-exporter.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab.gitlab-runner.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.gitlab-runner.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.gitlab-runner.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.gitlab-runner.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab-runner.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab-runner.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab-runner.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab-runner.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab.gitlab-shell.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.gitlab-shell.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.gitlab-shell.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.gitlab-shell.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab.migrations.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.migrations.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.migrations.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.migrations.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab.sidekiq.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.sidekiq.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.sidekiq.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.sidekiq.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab.task-runner.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.task-runner.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.task-runner.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.task-runner.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "gitlab.webservice.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "gitlab.webservice.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "gitlab.webservice.tolerations[0].effect"                                                  = "NoSchedule"
    "gitlab.webservice.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "shared-secrets.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "shared-secrets.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "shared-secrets.tolerations[0].effect"                                                  = "NoSchedule"
    "shared-secrets.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "nginx-ingress.controller.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "nginx-ingress.controller.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "nginx-ingress.controller.tolerations[0].effect"                                                  = "NoSchedule"
    "nginx-ingress.controller.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value

    "registry.tolerations[0].key"                                                     = local.derived_k8s_tolerations.key
    "registry.tolerations[0].value"                                                   = local.derived_k8s_tolerations.value
    "registry.tolerations[0].effect"                                                  = "NoSchedule"
    "registry.nodeSelector.${replace(local.derived_k8s_tolerations.key, ".", "\\.")}" = local.derived_k8s_tolerations.value
  }

  # External Database
  # According to https://docs.gitlab.com/charts/advanced/external-db/
  helm_psql_sets = {
    "host"            = module.db.db_instance_address
    "port"            = module.db.db_instance_port
    "database"        = module.db.db_instance_name
    "username"        = module.db.db_instance_username
    "password.secret" = kubernetes_secret.rds_password.metadata[0].name
    "password.key"    = local.rds_password_kubernetes_secret_key
  }

  # External Redis
  # According to https://docs.gitlab.com/charts/advanced/external-redis/
  helm_redis_sets = {
    "host"             = module.redis.endpoint
    "password.enabled" = "false"
  }

  # External Object Storage
  # According to https://docs.gitlab.com/charts/advanced/external-object-storage/
  helm_s3_buckets_sets = {
    "lfs.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["lfs"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "lfs.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "lfs.connection.key"    = local.object_store_connection_kubernetes_secret_key

    "artifacts.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["artifacts"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "artifacts.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "artifacts.connection.key"    = local.object_store_connection_kubernetes_secret_key

    "uploads.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["uploads"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "uploads.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "uploads.connection.key"    = local.object_store_connection_kubernetes_secret_key

    "packages.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["packages"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "packages.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "packages.connection.key"    = local.object_store_connection_kubernetes_secret_key

    "backups.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["backups"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "backups.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "backups.connection.key"    = local.object_store_connection_kubernetes_secret_key

    "externalDiffs.enabled"           = "true"
    "externalDiffs.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["externalDiffs"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "externalDiffs.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "externalDiffs.connection.key"    = local.object_store_connection_kubernetes_secret_key

    "pseudonymizer.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["pseudonymizer"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "pseudonymizer.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "pseudonymizer.connection.key"    = local.object_store_connection_kubernetes_secret_key

    "dependencyProxy.enabled"           = "true"
    "dependencyProxy.bucket"            = element(regex("(\\S*${local.s3_bucket_fragments["dependencyproxy"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "dependencyProxy.connection.secret" = kubernetes_secret.gitlab_object_store_connection.metadata[0].name
    "dependencyProxy.connection.key"    = local.object_store_connection_kubernetes_secret_key

  }

  # OmniAuth Configuration
  # According to https://docs.gitlab.com/charts/charts/globals.html#omniauth
  # Also see:
  # * https://medium.com/mop-developers/how-to-set-up-gitlab-single-sign-on-with-google-g-suite-f5e88ae8ba7
  # * https://docs.gitlab.com/ee/integration/saml.html
  omniauth_provider_id = "saml"
  helm_omniauth_sets = {
    "enabled" = var.omniauth_enabled
    //    "autoSignInWithProvider"  = local.omniauth_provider_id
    "syncProfileFromProvider[0]" = local.omniauth_provider_id
    "syncProfileAttributes[0]"   = "email"
    "allowSingleSignOn[0]"       = local.omniauth_provider_id
    "blockAutoCreatedUsers"      = "false"
    "autoLinkSamlUser"           = "true"
    "allowBypassTwoFactor[0]"    = local.omniauth_provider_id
    "providers[0].secret"        = var.omniauth_enabled ? kubernetes_secret.gitlab_omniauth_provider_saml[0].metadata[0].name : ""
    "providers[0].key"           = local.omniauth_provider_kubernetes_secret_key
  }

  # Nginx
  helm_nginx_internal_sets = {
    "nginx-ingress.controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-internal" = "true"
  }

  smtp_enabled = length(var.smtp_config) != 0

  helm_smtp_sets = {
    "enabled"             = local.smtp_enabled
    "address"             = local.smtp_enabled ? lookup(element(var.smtp_config, 0), "address", "") : ""
    "port"                = local.smtp_enabled ? lookup(element(var.smtp_config, 0), "port", "") : ""
    "user_name"           = local.smtp_enabled ? lookup(element(var.smtp_config, 0), "user_name", "") : ""
    "password.secret"     = local.smtp_enabled ? kubernetes_secret.gitlab_smtp[0].metadata[0].name : ""
    "password.key"        = local.smtp_kubernetes_secret_key
    "domain"              = local.smtp_enabled ? lookup(element(var.smtp_config, 0), "domain", "") : ""
    "authentication"      = local.smtp_enabled ? lookup(element(var.smtp_config, 0), "authentication", "") : ""
    "starttls_auto"       = local.smtp_enabled ? lookup(element(var.smtp_config, 0), "starttls_auto", "") : ""
    "openssl_verify_mode" = local.smtp_enabled ? lookup(element(var.smtp_config, 0), "openssl_verify_mode", "") : ""
  }

  helm_cron_backup_sets = {
    # Backups
    "enabled"                   = "true"
    "schedule"                  = "0 12 * * *"
    "extraArgs"                 = local.use_external_postgres ? "${local.backup_skip_components} --skip db" : local.backup_skip_components
    "resources.requests.cpu"    = "2"
    "resources.requests.memory" = "4096M"
    "resources.limits.cpu"      = "4"
    "resources.requests.memory" = "8192M"
    "persistence.enabled"       = "true"
    "persistence.size"          = "${var.backup_temporary_persistence_volume_size_in_gigabytes}Gi"
  }

  helm_appconfig_cron_jobs_sets = {
    "pipeline_schedule_worker.cron" = "*/5 * * * *"
  }

  helm_gitlab_runner_sets = {
    "concurrent"    = "100"
    "checkInterval" = "10"

    "logLevel" = var.gitlab_runner_log_level

    "runners.config" = var.gitlab_runner_config

    "runners.imagePullPolicy"   = "always"
    "runners.cache.cacheType"   = "s3"
    "runners.cache.cachePath"   = "gitlab_runner"
    "runners.cache.cacheShared" = "true"

    "runners.cache.s3ServerAddress"  = "s3.amazonaws.com"
    "runners.cache.s3BucketName"     = element(regex("(\\S*${local.s3_bucket_fragments["cache"]}\\S*)", join(" ", values(aws_s3_bucket.bucket)[*].id)), 0)
    "runners.cache.s3BucketLocation" = data.aws_region.current.name
    "runners.cache.s3CacheInsecure"  = "false"
    "runners.cache.secretName"       = kubernetes_secret.gitlab_runner_s3_access.metadata[0].name

    "runners.builds.cpuLimit"                          = var.gitlab_runner_build_resources.cpuLimit
    "runners.builds.memoryLimit"                       = var.gitlab_runner_build_resources.memoryLimit
    "runners.builds.cpuRequests"                       = var.gitlab_runner_build_resources.cpuRequests
    "runners.builds.memoryRequests"                    = var.gitlab_runner_build_resources.memoryRequests
    "runners.builds.cpuLimitOverwriteMaxAllowed"       = var.gitlab_runner_max_override_resources.cpuLimit
    "runners.builds.memoryLimitOverwriteMaxAllowed"    = var.gitlab_runner_max_override_resources.memoryLimit
    "runners.builds.cpuRequestsOverwriteMaxAllowed"    = var.gitlab_runner_max_override_resources.cpuRequests
    "runners.builds.memoryRequestsOverwriteMaxAllowed" = var.gitlab_runner_max_override_resources.memoryRequests

    "runners.services.cpuLimit"       = var.gitlab_runner_services_resources.cpuLimit
    "runners.services.memoryLimit"    = var.gitlab_runner_services_resources.memoryLimit
    "runners.services.cpuRequests"    = var.gitlab_runner_services_resources.cpuRequests
    "runners.services.memoryRequests" = var.gitlab_runner_services_resources.memoryRequests

    "runners.helpers.cpuLimit"       = var.gitlab_runner_helpers_resources.cpuLimit
    "runners.helpers.memoryLimit"    = var.gitlab_runner_helpers_resources.memoryLimit
    "runners.helpers.cpuRequests"    = var.gitlab_runner_helpers_resources.cpuRequests
    "runners.helpers.memoryRequests" = var.gitlab_runner_helpers_resources.memoryRequests

    "runners.privileged"  = var.gitlab_runner_priviliged
    "runners.pollTimeout" = var.gitlab_runner_poll_timeout_seconds
    "runners.outputLimit" = local.derived_runner_output_limit_in_kilos

    "runners.podLabels.${replace(local.runner_spot_restrict_scaledown_label.key, ".", "\\.")}" = local.runner_spot_restrict_scaledown_label.value
  }

  helm_gitlab_runner_toleration_sets = {
    "runners.nodeSelector.${replace(local.derived_k8s_ci_tolerations.key, ".", "\\.")}" = local.derived_k8s_ci_tolerations.value

    "runners.nodeTolerations[0].key"    = local.derived_k8s_ci_tolerations.key
    "runners.nodeTolerations[0].value"  = local.derived_k8s_ci_tolerations.value
    "runners.nodeTolerations[0].effect" = "NoSchedule"
  }

}

# See: https://docs.gitlab.com/charts/installation/command-line-options.html
resource "helm_release" "gitlab" {
  chart      = "gitlab"
  name       = var.name
  namespace  = kubernetes_namespace.gitlab.metadata[0].name
  repository = "https://charts.gitlab.io/"
  version    = local.chart_version
  wait       = false

  dynamic "set" {
    for_each = local.helm_gitlab_sets
    content {
      name  = set.key
      value = set.value
    }
  }

  dynamic "set" {
    for_each = var.suffix != null ? [
    ""] : []
    content {
      name  = "global.hosts.hostSuffix"
      value = var.suffix
    }
  }

  dynamic "set" {
    for_each = local.use_external_postgres ? local.helm_psql_sets : {}
    content {
      name  = "global.psql.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = local.use_external_redis ? local.helm_redis_sets : {}
    content {
      name  = "global.redis.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = local.use_external_object_store ? local.helm_s3_buckets_sets : {}
    content {
      name  = "global.appConfig.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = var.omniauth_enabled ? local.helm_omniauth_sets : {}
    content {
      name  = "global.appConfig.omniauth.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = local.helm_cron_backup_sets
    content {
      name  = "gitlab.task-runner.backups.cron.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = length(var.k8s_toleration_label) != 0 ? local.helm_toleration_sets : {}
    content {
      name  = set.key
      value = set.value
    }
  }

  dynamic "set" {
    for_each = var.use_internal_ingress ? local.helm_nginx_internal_sets : {}
    content {
      name  = set.key
      value = set.value
    }
  }

  dynamic "set" {
    for_each = local.helm_gitlab_runner_sets
    content {
      name  = "gitlab-runner.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = length(var.k8s_ci_toleration_label) > 0 ? local.helm_gitlab_runner_toleration_sets : {}
    content {
      name  = "gitlab-runner.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = length(var.smtp_config) > 0 ? local.helm_smtp_sets : {}
    content {
      name  = "global.smtp.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = local.helm_appconfig_cron_jobs_sets
    content {
      name  = "global.appConfig.cron_jobs.${set.key}"
      value = set.value
    }
  }

  timeout = 600

}

