variable "name" {
  type    = string
  default = "gitlab"
}

variable "suffix" {
  type    = string
  default = null
}

variable "domain" {
  type = string
}

variable "db_instance_class" {
  type    = string
  default = "db.t3.medium"
}

variable "rds_subnet_ids" {
  type = list(string)
}

variable "rds_database_name" {
  type    = string
  default = "gitlab"
}

variable "license_key" {
  type = string
}

variable "eks_wokers_security_group_id" {
  type = string
}

variable "redis_subnet_ids" {
  type = list(string)
}

variable "redis_instance_type" {
  type    = string
  default = "cache.t3.medium"
}

variable "omniauth_enabled" {
  type    = bool
  default = false
}

variable "idp_fingerprint" {
  type    = string
  default = ""
}

variable "idp_sso_target_url" {
  type    = string
  default = ""
}

variable "certmanager_issuer_email" {
  type = string
}

variable "k8s_toleration_label" {
  type = list(object({
    key : string,
    value : string
  }))

  default = []
}

variable "k8s_ci_toleration_label" {
  type = list(object({
    key : string,
    value : string
  }))

  default = []
}

variable "smtp_config" {
  type = list(object({
    address : string,
    port : number,
    user_name : string,
    password : string,
    domain : string,
    authentication : string,
    starttls_auto : string,
    openssl_verify_mode : string
  }))
  default = []
}

variable "gitaly_storage_size_gigabytes" {
  type    = number
  default = 200
}

variable "use_internal_ingress" {
  type    = bool
  default = false
}

variable "gitlab_runner_log_level" {
  description = "Set minimum log level for the Runner."
  type        = string
  default     = "info"
}

variable "gitlab_runner_poll_timeout_seconds" {
  type    = number
  default = 600
}

variable "gitlab_runner_build_resources" {
  type = object({
    cpuLimit : string,
    // has to be string since it can contain "m"
    memoryLimit : string,
    cpuRequests : string,
    memoryRequests : string
  })
  default = {
    "cpuLimit"       = "4"
    "memoryLimit"    = "16Gi"
    "cpuRequests"    = "1"
    "memoryRequests" = "2Gi"
  }
}

variable "gitlab_runner_services_resources" {
  type = object({
    cpuLimit : string,
    // has to be string since it can contain "m"
    memoryLimit : string,
    cpuRequests : string,
    memoryRequests : string
  })
  default = {
    "cpuLimit"       = "2"
    "memoryLimit"    = "4Gi"
    "cpuRequests"    = "200m"
    "memoryRequests" = "256Mi"
  }
}

variable "gitlab_runner_helpers_resources" {
  type = object({
    cpuLimit : string,
    // has to be string since it can contain "m"
    memoryLimit : string,
    cpuRequests : string,
    memoryRequests : string
  })
  default = {
    "cpuLimit"       = "2"
    "memoryLimit"    = "4Gi"
    "cpuRequests"    = "200m"
    "memoryRequests" = "256Mi"
  }

}

variable "gitlab_runner_priviliged" {
  type    = bool
  default = false
}

variable "gitlab_runner_max_override_resources" {
  type = object({
    cpuLimit : string,
    // has to be string since it can contain "m"
    memoryLimit : string,
    cpuRequests : string,
    memoryRequests : string
  })
  default = {
    "cpuLimit"       = "16"
    "memoryLimit"    = "64Gi"
    "cpuRequests"    = "16"
    "memoryRequests" = "64Gi"
  }
}

variable "gitlab_runner_config" {
  type    = string
  default = ""
}

variable "backup_temporary_persistence_volume_size_in_gigabytes" {
  type    = number
  default = 10
}