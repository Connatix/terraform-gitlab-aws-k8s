locals {
  templates_dir                                 = "${path.module}/templates"
  chart_version                                 = "4.11.3"
  postgres_version_full                         = "12.5"
  derived_postgres_version_major                = join(".", slice(split(".", local.postgres_version_full), 0, 1))
  rds_password_kubernetes_secret_key            = "rdspwd"
  root_password_kubernetes_secret_key           = "rootpwd"
  redis_password_kubernetes_secret_key          = "redispwd"
  license_kubernetes_secret_key                 = "license"
  object_store_connection_kubernetes_secret_key = "connection"
  omniauth_provider_kubernetes_secret_key       = "provider"
  smtp_kubernetes_secret_key                    = "smtp-password"

  use_external_postgres     = true
  use_external_redis        = true
  use_external_object_store = true

  runner_output_limit_in_megabytes     = 64
  derived_runner_output_limit_in_kilos = local.runner_output_limit_in_megabytes * 1024

  runner_spot_restrict_scaledown_label = {
    key : "spotinst.io/restrict-scale-down",
    value : "true"
  }

  backup_skip_components = "--skip registry --skip artifacts --skip external_diffs"
}
