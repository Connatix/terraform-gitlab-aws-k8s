terraform {
  required_providers {
    aws        = "> 2.63"
    kubernetes = "> 1.11.3"
    helm       = "> 2.0.1"
    random     = "> 2.2.1"
  }
}

data "aws_region" "current" {
  provider = aws
}
