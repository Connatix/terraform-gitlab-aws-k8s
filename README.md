# GitLab on AWS & Kubernetes - An opinionated installation

This module is a highly opinionated installation of *GitLab* which makes use of the official 
[GitLab Helm Chart](https://docs.gitlab.com/charts/) to install and configure GitLab on an arbitrary Kubernetes cluster
using [Terraform](https://www.terraform.io/), and creating all the necessary external dependencies in a provided AWS 
account, achieving a high level of automation and easy maintainability.

## Structure

There are two directories in the repository:
* *examples* - containing an example of invocation from terraform
* *gitlab* - the terraform module that should be reusable across environments

## AWS Specific Configuration

GitLab offers the possibility to disable certain dependency pods and utilize externally hosted services instead.
Such services are:
* Object Storage - Instead of MinIO which is not production ready, S3 buckets can be utilized
* PostgreSQL - Instead of a statefulset, AWS RDS Postgres will be used
* Redis - Instead of a redis pod, AWS ElastiCache will be used // TODO - not yet implemented

The module will take create and maintain the above services by with Terraform, there is no manual configuration needed. 

## Helm Charts

This module is using the official Helm chart offered by GitLab, hosted on `https://charts.gitlab.io/` and maintained by
the GitLab team. We are trying to update it regularly and do our best to ensure it works in our opinionated setup.

The following diagram is provided by GitLab

![alt text](https://gitlab.com/gitlab-org/charts/gitlab/-/raw/master/doc/images/charts.png "Charts")

We deliberately disable Postgres and Redis and move them in AWS managed services.

## Installation Overview

All kubernetes resources created by this module are going to be placed in a single namespace controlled by the `name` 
variable and being defaulted to `gitlab`.

The services deployed by the module are as follows:
* gitlab-cert-manager - used for Let's Encrypt certificate acquisition & management
* gitlab-gitaly - GitLab's RPC over Git implementation
* gitlab-gitlab-exporter
* gitlab-gitlab-shell
* gitlab-nginx-ingress-controller - an NGINX ingress controller. 
* gitlab-nginx-ingress-default-backend
* gitlab-redis-master - Redis # TODO should run in ElastiCache
* gitlab-registry - Docker compatible registry implementation
* gitlab-unicorn - main web service of GitLab

`Note: it's preferred to run GitLab behind it's own Ingress controller since it needs to expose port 22 (SSH) for Git 
over SSH, so we need a slightly modified Ingress Controller which allows passthrough of such traffic.`

## High Availability

For a production ready setup it's recommended to have services of a certain kind (pods of a deployment in k8s) running
on different VMs machines to reduce the risk of downtime. This module automatically ensures that the pods have hard
affinity upon scheduling, resulting in the aforementioned behavior.

See: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity

## CI/CD

GitLab offers various types of executors to run the `GitLab Runner` - the component used to execute CI/CD pipelines and 
jobs.

This module automatically registers the Kubernetes cluster where the solution is deployed to as a 
valid & shared Kubernetes Executor.
For documentation of the the k8s executor see: https://docs.gitlab.com/runner/executors/kubernetes.html

We can add a custom toleration and node selector to the deployment by using the `k8s_ci_toleration_label` variable 
of the module. 
A common pattern is to taint the nodes that run your CI workloads to prevent "regular" or non-CI pods 
scheduling on them. Another common pattern is to run your CI workloads on spot instances to significanlty reduce costs.
With the same label being used as nodeSelector, we make sure to offer the possibility to keep costs to a minimum.

The GitLab CI pods can have multiple containers:
- The build container, named build
- The helper container, named helper
- The services containers are named svc-X (X being a number)

You can configure the resource reservation and limits for these individually by setting the
`gitlab_runner_builder_resources`, `gitlab_runner_helpers_resources`, `gitlab_runner_services_resources` variables.

These can also be overriden on a per-pipeline basis in the `.gitlab-ci.yml`.
```yml
variables:
   KUBERNETES_CPU_REQUEST: 3
   KUBERNETES_CPU_LIMIT: 5
   KUBERNETES_MEMORY_REQUEST: 2Gi
   KUBERNETES_MEMORY_LIMIT: 4Gi
``` 
For more details see the documentation at https://docs.gitlab.com/runner/executors/kubernetes.html#overwriting-build-resources.

Runner pods are prefix with `runner-*` and for simplicity are started in the same namespace as the main 
installation(defaults to `gitlab`).

## SSO - OmniAuth Configuration
There is support for confiugring multiple identity providers with GitLab's OmniAuth.

Currently the module supports configuring a single SAML provider.

For full documentation of OmniAuth see https://docs.gitlab.com/ee/integration/saml.html.   

## Used Terraform Providers
* [AWS](https://www.terraform.io/docs/providers/aws/index.html) - Creating AWS resources
* [Kubernetes](https://www.terraform.io/docs/providers/kubernetes/index.html) - Creating namespaces, secrets
* [Helm](https://www.terraform.io/docs/providers/helm/index.html) - Performing the release
* [Random](https://www.terraform.io/docs/providers/random/index.html) - Creating IDs and passwords

# Operating considerations

### SSL
The nginx Ingress Controller from the chart is configured to get valid and trusted SSL certificates from Let's Encrypt 
using Certmanager. 
As long as there are valid and publicly resolvable DNS records pointing to the ingress controller, certificate issuing
and rotation should happen automatically without manual intervention.

You can set the variable `certmanager_issuer_email` to configure your SRE teams' email address.

### Backups
GitLab has utilities for backing up and restoring an installation.
For a generic description of this functionality check [this](https://docs.gitlab.com/ee/raketasks/backup_restore.html) link.

For a kubernetes specific guide see 
[this](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/backup-restore/backup.md#backing-up-a-gitlab-installation) 
link.

> **WARNING**
>
> Make sure to back up the rails secret separately since it is not included in the backup tarball.
> Recommended to keep it separately from your backup, e.g. 1Password or similar.
>

### Kubernetes PVC related operations

Storage changes after installation need to be manually handled by your cluster administrators. Automated management of 
these volumes after installation is *NOT* handled by the GitLab chart.

For more information see [this](https://docs.gitlab.com/charts/advanced/persistent-volumes/) link.
